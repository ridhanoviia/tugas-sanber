@extends('master')

@section('content')
<div class="card-header">
    <h3 class="card-title">Pemasok Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success')}}
            </div>
        @endif
    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="width: 10px">#</th>
            <th style="width: 35%; text-align: center">Nama</th>
            <th style="text-align: center">Email</th>
            <th style="text-align: center">No. Handphone</th>
            <th style="text-align: center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pemasok as $key => $pemasok)
            <tr>
                <td> {{ $key + 1 }} </td>
                <td> {{ $pemasok -> nama }} </td>
                <td> {{ $pemasok -> email }} </td>
                <td> {{ $pemasok -> phone }} </td>
                <td style="display:flex;">
                    <a href="/pemasok/{{$pemasok->id}}" class="btn btn-info btn-sm">show</a>
                    <a href="/pemasok/{{$pemasok->id}}/edit" class="btn btn-default btn-sm">edit</a> 
                    <form action="/pemasok/{{$pemasok->id}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form> 
                </td>       
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@endsection
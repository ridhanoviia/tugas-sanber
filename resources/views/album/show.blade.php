@extends('master')

@section('content')
<div class="ml-4 mt-2 mr-4"> 
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Show Album Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center">ID</th>
                <th style="width: 35%; text-align: center">Nama</th>
                <th style="text-align: center">Penyanyi</th>
                <th style="text-align: center">Harga</th>
                <th style="text-align: center">Deskripsi</th>
                <th style="text-align: center">Pemasok ID</th>
                
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{ $album -> id }} </td>
                    <td> {{ $album -> nama }} </td>
                    <td> {{ $album  -> penyanyi }} </td>
                    <td> {{ $album  -> harga }} </td>
                    <td> {{ $album  -> deskripsi }} </td>
                    <td> {{ $album  -> pemasok_id }} </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
    <!-- /.card -->
</div>
@endsection
@extends('master')

@section('content')
<div class="ml-4 mt-2 mr-4"> 
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Edit Pemasok</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/album/{{$album->id}}" method="POST">
        {{ csrf_field() }}
        @method('PUT')
        <div class="card-body">
            <div class="form-group row">
                <label for="nama" class="col-sm-2 text-right control-label col-form-label">Nama</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="nama" name="nama" value="{{$album->nama}}" placeholder="Nama Album">
                </div>
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group row">
                <label for="penyanyi" class="col-sm-2 text-right control-label col-form-label">Penyanyi</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="penyanyi" name="penyanyi" value="{{$album->penyanyi}}" placeholder="Nama Penyanyi">
                </div>
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group row">
                <label for="harga" class="col-sm-2 text-right control-label col-form-label">Harga</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <input type="text" class="form-control" id="harga" name="harga" value="{{$album->harga}}" placeholder="..."  aria-describedby="basic-addon2">
                        <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">Rp</span>
                        </div>
                    </div>
                </div>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group row">
                <label for="deskripsi" class="col-sm-2 text-right control-label col-form-label">Deskripsi</label>
                <div class="col-sm-9">
                    <textarea class="form-control" id="deskripsi" name="deskripsi" value="{{$album->deskripsi}}" placeholder="Deskripsi Album"></textarea>
                </div>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>

           
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
        </div> 
        </form>
    </div>
    <!-- /.card -->
</div>
@endsection
@extends('master')


@section('content')
<form class="form-horizontal" role="form" action="/profile" method="POST">
    {{csrf_field()}}
    <div class="form-group row">
        <label for="nama" class="col-sm-3 text-right control-label col-form-label"> Name</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Name Here">
        </div>
    </div>
    <div class="form-group row">
        <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email" placeholder="Your Email Here">
        </div>
    </div>
    <div class="form-group row">
        <label for="pass" class="col-sm-3 text-right control-label col-form-label">Password</label>
        <div class="col-sm-9">
            <input type="password" class="form-control" id="pass" name="pass" placeholder="Password Here">
        </div>
    </div>
    <div class="form-group row">
        <label for="notel" class="col-sm-3 text-right control-label col-form-label">No. Handphone</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="notel" name="notel" placeholder="Contact No Here">
        </div>
    </div>
    <div class="form-group row">
        <label for="alamat" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="alamat" id="alamat"></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="jenis_kelamin" class="col-sm-3 text-right control-label col-form-label">Jenis Kelamin</label>
        <div class="col-md-9">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="Pria" value="Pria" name="jenis_kelamin" required>
                <label class="custom-control-label" for="Pria">Pria</label>
            </div>
                <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="Wanita" value="Wanita" name="jenis_kelamin" required>
                <label class="custom-control-label" for="Wanita">Wanita</label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="ttl" class="col-sm-3 text-right control-label col-form-label">TTL</label>
        <div class="col-md-9">
            <input type="text" class="form-control date-inputmask" id="ttl" name="ttl" placeholder="Enter Date">
        </div>
    </div>
    <div class="form-group row">
        <div class="card-body">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection       

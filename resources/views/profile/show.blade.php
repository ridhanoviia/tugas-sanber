@extends('master')

@section('content')
<div>
    <form>
        <div class="profile-header">
            <h3>
                <b></b> YOUR PROFILE <br>
                <small>This information will let us know more about you.</small>
            </h3>
            <hr>
        </div>

        <div class="clear">
            <div class="col-sm-3 padding-top-25">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th style="width: 35%; text-align: center">Nama</th>
                        <th style="text-align: center">Email</th>
                        <th style="text-align: center">No. Telepon</th>
                        <th style="text-align: center">Alamat</th>
                        <th style="text-align: center">Tanggal Lahir</th>
                        <th style="text-align: center">User ID</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> {{ $profile -> id }} </td>
                            <td> {{ $profile -> nama }} </td>
                            <td> {{ $profile -> email }} </td>
                            <td> {{ $profile -> notel }} </td>
                            <td> {{ $profile -> alamat }} </td>
                            <td> {{ $profile -> ttl }} </td>
                            <td> {{ $profile -> user_id }} </td>    
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
</form>
</div>
@endsection       
            
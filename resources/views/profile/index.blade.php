@extends('master')

@section('content')
<div>
    <form>
        <div class="card-header">
            <h3>
                <b></b> YOUR PROFILE <br>
                <small>This information will let us know more about you.</small>
            </h3>
            <hr>
        </div>

        <div class="clear">
            <div class="col-sm-3 padding-top-25">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th style="width: 35%; text-align: center">Nama</th>
                        <th style="text-align: center">Email</th>
                        <th style="text-align: center">No. Telepon</th>
                        <th style="text-align: center">Alamat</th>
                        <th style="text-align: center">Tanggal Lahir</th>
                        <th style="text-align: center">User ID</th>
                        <th style="text-align: center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($profile as $key => $profile)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $profile -> nama }} </td>
                            <td> {{ $profile -> email }} </td>
                            <td> {{ $profile -> notel }} </td>
                            <td> {{ $profile -> alamat }} </td>
                            <td> {{ $profile -> ttl }} </td>
                            <td> {{ $profile -> user_id }} </td>
                            <td style="display:flex;">
                                <a href="/profile/{{$profile->id}}" class="btn btn-info btn-sm">show</a>
                                <a href="/profile/{{$profile->id}}/edit" class="btn btn-default btn-sm">edit</a> 
                                <form action="/profile/{{$profile->id}}" method="post">
                                {{ csrf_field() }}
                                @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form> 
                            </td>       
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <a href="/profile/create" class="btn btn-primary my-2">Tambah</a>
</form>
</div>
@endsection       
            
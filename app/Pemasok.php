<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemasok extends Model
{
    public $timestamps = false;
    protected $table = "pemasok";
    protected $guarded = [];
}

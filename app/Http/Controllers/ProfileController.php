<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::all();
        return view('profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'pass'  => 'required',
            'notel' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'ttl' => 'required'
        ]);
        Profile::create([
            "nama" => $request->nama,
            "email" => $request->email,
            "pass" => $request->pass,
            "notel" => $request->notel,
            "alamat" => $request->alamat,
            "jenis_kelamin" => $request->jenis_kelamin,
            "ttl" => $request->ttl,
            //TO DO: ganti jadi user_id dari user yg login
            "user_id" => 1
        ]);
        return redirect('/profile')->with('success', 'Profile berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        return view('profile.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Profile::where('id', $id)->update([
            "nama" => $request["nama"],
            "email" => $request["email"],
            "pass" => $request["pass"],
            "notel" => $request["notel"],
            "alamat" => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "ttl" => $request["ttl"]
        ]);
        return redirect('/profile')->with('success', 'Data sukses diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profile::destroy($id);
        return redirect('/profile');
    }
}
